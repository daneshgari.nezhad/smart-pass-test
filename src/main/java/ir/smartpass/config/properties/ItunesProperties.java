package ir.smartpass.config.properties;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.time.Duration;


@Component
@Data
@ConfigurationProperties("application.itunes")
@NoArgsConstructor
public class ItunesProperties {
    private Duration cacheTtl = Duration.ofMinutes(5);
    private Integer perPage;
}