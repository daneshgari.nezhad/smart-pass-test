package ir.smartpass.config.properties;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.time.Duration;


@Component
@Data
@ConfigurationProperties("application.google-book")
@NoArgsConstructor
public class GoogleBookProperties {
    private Integer perPage;
    private Duration cacheTtl = Duration.ofMinutes(5);
    private String apiKey;
}