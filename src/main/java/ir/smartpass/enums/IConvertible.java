package ir.smartpass.enums;

public interface IConvertible<T> {
    T getValue();
}
