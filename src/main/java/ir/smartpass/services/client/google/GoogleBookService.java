package ir.smartpass.services.client.google;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import ir.smartpass.dto.external.googleBook.response.BookResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


@Service
@Slf4j
public class GoogleBookService {
    private final RestTemplate restTemplate;
    private final String googleBooksApiBaseUrl;
    private final String googleBooksApiKey;

    public GoogleBookService(@Value("${application.google-book.base-url}") String googleBooksApiBaseUrl,
                             @Value("${application.google-book.api-key}") String googleBooksApiKey, @Qualifier("restTemplate") RestTemplate restTemplate) {
        this.googleBooksApiBaseUrl = googleBooksApiBaseUrl;
        this.googleBooksApiKey = googleBooksApiKey;
        this.restTemplate = restTemplate;
    }

    @CircuitBreaker(name = "myCircuitBreaker", fallbackMethod = "circuitBreakerFallback")
    @RateLimiter(name = "myRateLimiter", fallbackMethod = "rateLimitFallback")
    public BookResult searchBooks(String searchTerm) {
        log.info("service:GoogleBookService,method:searchBooks, searchTerm : " + searchTerm);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(googleBooksApiBaseUrl)
                .path("/volumes")
                .queryParam("q", searchTerm)
                .queryParam("key", googleBooksApiKey)
                .queryParam("orderBy", "relevance");
        return restTemplate.getForObject(builder.toUriString(), BookResult.class);
    }

    public void circuitBreakerFallback(Exception e) {
        log.error("call googleBook webClient make error:" + e.getMessage());
        e.printStackTrace();
    }

    public void rateLimitFallback(Exception e) {
        log.error("call googleBook webClient make error:" + e.getMessage());
        e.printStackTrace();
        log.error("");
    }
}

