package ir.smartpass.services.client.itunes;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import ir.smartpass.dto.external.itunes.response.MusicResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
@Slf4j
public class AlbumService {
    private final String itunesApiBaseUrl;

    public AlbumService(@Value("${application.itunes.base-url}") String itunesApiBaseUrl) {
        this.itunesApiBaseUrl = itunesApiBaseUrl;
    }

    @CircuitBreaker(name = "myCircuitBreaker", fallbackMethod = "circuitBreakerFallback")
    @RateLimiter(name = "myRateLimiter", fallbackMethod = "rateLimitFallback")
    public MusicResult searchAlbums(String searchTerm) {
        log.info("service:AlbumService ,method:save, searchAlbums : " + searchTerm, "desc: call itunes service");
        MusicResult musicResult = null;
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(itunesApiBaseUrl)
                .path("/search")
                .queryParam("entity", "album")
                .queryParam("term", searchTerm)
                .queryParam("sort", "album");

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(builder.toUriString()))
                .GET()
                .build();

        try {
            HttpResponse<String> response = httpClient.send(httpRequest,
                    HttpResponse.BodyHandlers.ofString());
            int statusCode = response.statusCode();
            if (statusCode == 200) {
                String responseBody = response.body();
                ObjectMapper objectMapper = new ObjectMapper();
                musicResult = objectMapper.readValue(responseBody, MusicResult.class);

            } else {
                log.info("service:AlbumService ,method:save, searchAlbums : " + searchTerm, "desc: called itunes service,with status code: " + statusCode);
            }
        } catch (Exception e) {
            log.error("service:AlbumService ,method:save, searchAlbums : " + searchTerm, "desc: " + e.getMessage());
            e.printStackTrace();
        }
        return musicResult;
    }

    public void circuitBreakerFallback(Exception e) {
        log.error("call Itunes webClient make error:" + e.getMessage());
        e.printStackTrace();
    }

    public void rateLimitFallback(Exception e) {
        log.error("call Itunes webClient make error:" + e.getMessage());
        e.printStackTrace(); log.error("");
    }


}