package ir.smartpass.services.interfaces;

import ir.smartpass.dto.external.SearchMediaResultDto;

import java.util.List;

public interface IMediaService {
    List<SearchMediaResultDto> searchMedia(String searchTerm);
}
