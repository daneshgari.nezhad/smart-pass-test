package ir.smartpass.dto.external.itunes.response;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MusicResult {
    private Integer resultCount;
    private List<MusicItem> results = new ArrayList<>();
}
