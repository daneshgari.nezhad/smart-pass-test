package ir.smartpass.repository;

import ir.smartpass.config.properties.ItunesProperties;
import ir.smartpass.dto.external.itunes.response.MusicResult;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

@Component
public class MusicCacheRepository {
    private static final String PREFIX = "Music_";
    private final ValueOperations<String, MusicResult> valueOps;
    private final RedisTemplate<String, MusicResult> redisTemplate;
    private final ItunesProperties itunesProperties;

    public MusicCacheRepository(
            @Qualifier("musicTemplate") RedisTemplate<String, MusicResult> redisTemplate,
            ItunesProperties itunesProperties
    ) {
        this.valueOps = redisTemplate.opsForValue();
        this.redisTemplate = redisTemplate;
        this.itunesProperties = itunesProperties;
    }

    public MusicResult get(String key) {
        String fullKey = getFullKey(key);
        return valueOps.get(fullKey);
    }

    private String getFullKey(String key) {
        return PREFIX + key;
    }

    public boolean save(String key, MusicResult musicResult) {
        String fullKey = getFullKey(key);
        valueOps.set(fullKey, musicResult, itunesProperties.getCacheTtl());
        return true; // Return a result, e.g., success or failure.
    }
}
